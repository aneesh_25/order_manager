interface Order {
    address?: {
      street?: string,
      city?: string,
      country?: string,
      zip?: string
    },
    customer?: {
      name?: string,
      email?: string,
      phone?: string
    },
    bookingDate: number,
    title: string,
    id: string
}
export default Order;