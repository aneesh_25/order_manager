import React from 'react';
import './App.css';
import { Container } from 'react-bootstrap';
import Login from './pages/Login';
import { Router, Route, Switch } from 'react-router-dom';
import Orders from './pages/Orders';
import UpdateOrder from './pages/UpdateOrder';
import AddOrder from './pages/AddOrder';
import PrivateRoute from './components/PrivateRoute';
import NavBar from './components/NavBar';
import history from './core/history';
import {NotificationContainer } from 'react-notifications';

function App() {
  return (
    <Container>
      <Router history={history}>
        <NavBar/>
        <Switch>
          <PrivateRoute path="/orders/add" component={AddOrder}>
          </PrivateRoute>
          <PrivateRoute path="/orders/:id" component={UpdateOrder}>
          </PrivateRoute>
          <PrivateRoute path="/orders" component={Orders}/>
          <Route path="/">
            <Login />
          </Route>
        </Switch>
      </Router>
      <NotificationContainer/>
    </Container>
  );
}

export default App;
