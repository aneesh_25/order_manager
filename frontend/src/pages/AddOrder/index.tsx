import Order from 'models';
import { connect} from 'react-redux';
import { Dispatch } from 'redux';
import { addOrder } from '../../core/actions/orders.action';
import AddOrder from './AddOrder';


const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        addOrder: (order: Order) => {
            dispatch(addOrder(order))
        }
    }
}
export default connect(null, mapDispatchToProps)(AddOrder)