import { fireEvent } from "@testing-library/react";
import { ComponentProps } from "react";
import { renderWithStore } from "../../test.utils";
import AddOrder from "./AddOrder";

describe('Add Order Page', () => {
    let props: ComponentProps<typeof AddOrder>
    beforeEach(() => {
        props = {
            addOrder: jest.fn(),
        }
    })
    it('User should be able to add an order', () => {
        const { getByPlaceholderText, getByRole } = renderWithStore(<AddOrder {...props}/>)
        fireEvent.change(getByPlaceholderText('Title'), {
            target: { value: 'Title'}
        })
        fireEvent.change(getByPlaceholderText('Customer Name'), {
            target: { value: 'Name'}
        })
        fireEvent.change(getByPlaceholderText('Customer Email'), {
            target: { value: 'Email'}
        })
        fireEvent.change(getByPlaceholderText('City'), {
            target: { value: 'City'}
        })
        fireEvent.click(getByRole('button', { name: 'Submit'}));
        expect(props.addOrder).toHaveBeenCalled();
        expect(props.addOrder).toHaveBeenCalledWith( {"address": {"city": "City"}, "customer": {"email": "Email", "name": "Name"}, "title": "Title"});
    });
})