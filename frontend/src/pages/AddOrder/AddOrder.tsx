import Order from "models"
import React from "react"
import EditOrder from "../../components/Partials/EditOrders"

type Props = {
    addOrder(order: Order) : void;
}
const AddOrder: React.FunctionComponent<Props> = ({ addOrder }) => {

    const onUpdate = (o: Order) => {
        addOrder(o)
    }
    return (
        <EditOrder order={{} as Order} onUpdate={onUpdate}/>
    )
}
export default AddOrder;