import { fireEvent } from "@testing-library/react";
import { ComponentProps } from "react";
import { renderWithStoreAndThemeAndRouter } from "../../test.utils";
import UpdateOrder from "./UpdateOrder";

describe('Add Order Page', () => {
    let props: ComponentProps<typeof UpdateOrder>
    beforeEach(() => {
        props = {
            order: { id: "1", bookingDate: 123, address: { "city": "City" }, "customer": { "email": "Email", "name": "Name" }, "title": "Title" },
            getOrder: jest.fn(),
            updateOrder: jest.fn(),
        }
    })
    it('order information should be prefilled', () => {
        const { getByPlaceholderText } = renderWithStoreAndThemeAndRouter(
            <UpdateOrder {...props} />)
        expect(getByPlaceholderText('Title')).toHaveValue('Title');
        expect(getByPlaceholderText('Customer Name')).toHaveValue('Name');
        expect(getByPlaceholderText('Customer Email')).toHaveValue('Email');
        expect(getByPlaceholderText('City')).toHaveValue('City');

    });
    it('User should be able to add an order', () => {
        const { getByPlaceholderText, getByRole } = renderWithStoreAndThemeAndRouter(
            <UpdateOrder {...props} />)
        fireEvent.change(getByPlaceholderText('Title'), {
            target: { value: 'Title2' }
        })
        fireEvent.change(getByPlaceholderText('Customer Name'), {
            target: { value: 'Name2' }
        })
        fireEvent.change(getByPlaceholderText('Customer Email'), {
            target: { value: 'Email2' }
        })
        fireEvent.change(getByPlaceholderText('City'), {
            target: { value: 'City2' }
        })
        fireEvent.click(getByRole('button', { name: 'Submit' }));
        expect(props.updateOrder).toHaveBeenCalled();
        expect(props.updateOrder).toHaveBeenCalledWith({ "id": "1", "bookingDate": 123, "address": { "city": "City2" }, "customer": { "email": "Email2", "name": "Name2" }, "title": "Title2" });
    });
})