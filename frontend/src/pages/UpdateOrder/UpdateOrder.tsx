import Order from "models"
import React, { useEffect } from "react"
import { useParams } from "react-router-dom"
import EditOrder from "../../components/Partials/EditOrders"

type Props = {
    order: Order,
    getOrder(id: string): void;
    updateOrder(order: Order) : void;
}
const UpdateOrder: React.FunctionComponent<Props> = ({ order, getOrder, updateOrder }) => {
    const { id } = useParams<{ id: string }>()
    useEffect(() => {
        getOrder(id);
    }, [id, getOrder]);

    const onUpdate = (o: Order) => {
        updateOrder(o)
    }
    return (
        <EditOrder order={order} onUpdate={onUpdate}/>
    )
}
export default UpdateOrder;