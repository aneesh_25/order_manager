import Order from 'models';
import { connect} from 'react-redux';
import { Dispatch } from 'redux';
import { getOrder, updateOrder } from '../../core/actions/orders.action';
import { RootState } from '../../core/reducers';
import UpdateOrder from './UpdateOrder';

const mapStateToProps = (state: RootState) => {
    return {
        order: state.orders.order || {} as Order
    }
}
const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        getOrder: (id: string) => {
            dispatch(getOrder(id));
        },
        updateOrder: (order: Order) => {
            dispatch(updateOrder(order))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UpdateOrder)