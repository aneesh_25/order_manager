import { connect} from 'react-redux';
import { Dispatch } from 'redux';
import { getOrders } from '../../core/actions/orders.action';
import { RootState } from '../../core/reducers';
import Orders from './Orders';

const mapStateToProps = (state: RootState) => {
    return {
        orders: state.orders.orders
    }
}
const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        getOrders: () => {
            dispatch(getOrders());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Orders)