import Order from 'models';
import React, { useEffect } from 'react';
import { Button, Table } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Address from '../../components/Address';
import Customer from '../../components/Customer';
import { selectToken } from '../../selectors';
import { NotificationManager} from 'react-notifications';

type Props = {
    orders: Order[],
    getOrders(): void;
};
const Orders: React.FunctionComponent<Props> = ({ orders, getOrders }) => {
    const history = useHistory();

    useEffect(() => {
        getOrders();
    }, [])
    return (<>
        <h3 className="my-sm-2">Orders</h3>
        <Button className="my-sm-2" onClick={() => history.push('/orders/add')}>Add order</Button>
        <Table striped bordered hover>
            <thead>
                <tr><td>Booking id</td>
                    <td>Title</td>
                    <td>Customer</td>
                    <td>Address</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
                {orders.map(order => <tr key={order.id}>
                    <td>{order.id}</td>
                    <td>{order.title}</td>
                    <td><Customer customer={order.customer} /></td>
                    <td><Address address={order.address} /></td>
                    <td><Button onClick={() => history.push(`/orders/${order.id}`)}>Edit</Button></td>
                </tr>)}
            </tbody>
        </Table>
    </>)
}
export default Orders;