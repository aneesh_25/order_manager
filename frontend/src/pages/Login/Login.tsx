import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';

type Props = {
  login: (username: string, password: string) => void;
}
const Login: React.FunctionComponent<Props> = ({ login }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    login(email, password);
  }
  return (
    <>
      <h3 style={{ textAlign: 'center', marginTop: '30px' }}> Order management</h3>
      <h2>Login</h2>
      <Form onSubmit={onSubmit}>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required />
        </Form.Group>

        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form></>)
}
export default Login;