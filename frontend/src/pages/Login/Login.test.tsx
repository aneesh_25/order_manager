import { fireEvent } from "@testing-library/react";
import { ComponentProps } from "react";
import { renderWithStore } from "../../test.utils";
import Login from "./Login";

describe('Login Page', () => {
    let props: ComponentProps<typeof Login>
    beforeEach(() => {
        props = {
            login: jest.fn()
        }
    })
    it('User should be able to enter details and login', () => {
        const { getByPlaceholderText, getByRole } = renderWithStore(<Login {...props}/>)
        fireEvent.change(getByPlaceholderText('Enter email'), {
            target: { value: 'a@k'}
        })
        fireEvent.change(getByPlaceholderText('Password'), {
            target: { value: 'password'}
        })
        fireEvent.click(getByRole('button', { name: 'Submit'}));
        expect(props.login).toHaveBeenCalled();
        expect(props.login).toHaveBeenCalledWith('a@k', 'password');
    });
})