import { connect} from 'react-redux';
import { Dispatch } from 'redux';
import { login } from '../../core/actions/login.action';
import Login from './Login';

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        login: (username: string, password: string) => {
            dispatch(login(username, password));
        }
    }
}
export default connect(null, mapDispatchToProps)(Login)