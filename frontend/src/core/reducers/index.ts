import { combineReducers } from 'redux';
import login from './login.reducer';
import orders from './orders.reducer';

const rootReducer =  combineReducers({
    login,
    orders
})
export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;