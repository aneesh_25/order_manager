import { LoginActionTypes, RESET_LOGIN, SET_LOGIN } from "../actions/login.action";
import produce from 'immer';

const INITIAL_STATE: {
    user: { username: string, token: string } | null
} = {
    user: null
}

const login = (state = INITIAL_STATE, action: LoginActionTypes) => {
    return produce(state, (draft) => {
        switch (action.type) {
            case SET_LOGIN:
                draft.user = {
                    username: action.username,
                    token: action.token
                }
                return draft
            case RESET_LOGIN:
                draft.user = null;
                return draft;
            default: return state;
        }
    })
}
export default login;