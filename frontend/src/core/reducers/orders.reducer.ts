import produce from 'immer';
import Order from "models";
import { OrderActionsTypes, SET_ORDER, SET_ORDERS } from "../actions/orders.action";

const INITIAL_STATE: { orders: Order[], order: Order | null } = {
    orders: [],
    order: null
}

const login = (state = INITIAL_STATE, action: OrderActionsTypes) => {
    return produce(state, (draft) => {
        switch (action.type) {
            case SET_ORDERS:
                draft.orders = action.orders;
                return draft;
            case SET_ORDER:
                draft.order = action.order;
                return draft;
            default: return state;
        }
    })
}
export default login;