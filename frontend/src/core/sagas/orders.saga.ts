import axios from 'axios';
import firebase from 'firebase';
import Order from 'models';
import { call, put, takeLatest} from 'redux-saga/effects'; 
import * as OrdersAction from '../actions/orders.action';
import { NotificationManager} from 'react-notifications';
import history from '../history';

export  function* watchGetOrders(){
    yield takeLatest(OrdersAction.GET_ORDERS, getOrders)
}
export  function* watchGetOrder(){
    yield takeLatest(OrdersAction.GET_ORDER, getOrder)
}
export  function* watchUpdateOrder(){
    yield takeLatest(OrdersAction.UPDATE_ORDER, updateOrder)
}

export  function* watchAddOrder(){
    yield takeLatest(OrdersAction.ADD_ORDER, addOrder)
}
function * getOrders() {
    const collection = firebase.firestore().collection('orders');
    const ordersData: firebase.firestore.QuerySnapshot<firebase.firestore.DocumentData> = yield call([collection, collection.get]);
    const orders: Order[] = []
    ordersData.forEach((doc) => {
        orders.push({ ...doc.data() as Order, id: doc.id})
    })
    yield put(OrdersAction.setOrders(orders));
}

function * getOrder(action: ReturnType<typeof OrdersAction.getOrder>) {
    const collection = firebase.firestore().collection('orders').doc(action.id);
    const ordersData: firebase.firestore.DocumentSnapshot<firebase.firestore.DocumentData> = yield call([collection, collection.get]);
    if(ordersData.exists){
        yield put(OrdersAction.setOrder({  ...ordersData.data() as Order, id: action.id }))
    }else{
        yield call([NotificationManager,NotificationManager.error ], 'Could not fetch order');
    }
}

function * updateOrder(action: ReturnType<typeof OrdersAction.updateOrder>){
    try {
       yield call(axios.put, `/api/orders/${action.order.id}`, action.order, {  headers: { Authorization: localStorage.getItem('token')!}})
       yield call(history.push, '/orders');
       yield call([NotificationManager,NotificationManager.success ], 'Success')
    } catch (error) {
        yield call([NotificationManager,NotificationManager.error ], 'Could not update order')
    }
}

function * addOrder(action: ReturnType<typeof OrdersAction.updateOrder>){
    try {
        yield call(axios.post, `/api/orders`, action.order, {  headers: { Authorization: localStorage.getItem('token')!}})
        yield call(history.push, '/orders');
    } catch (error) {
        yield call([NotificationManager,NotificationManager.error ], 'Could not add order')
    }
}