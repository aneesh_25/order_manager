import firebase from 'firebase';
import { call, put, takeLatest} from 'redux-saga/effects'; 
import * as LoginAction from '../actions/login.action';
import history from '../history';
import { NotificationManager} from 'react-notifications';

export  function* watchLogin(){
    yield takeLatest(LoginAction.LOGIN, login)
}
export  function* watchLogout(){
    yield takeLatest(LoginAction.LOGOUT, logout)
}
export function* watchInit() {
    yield takeLatest(LoginAction.INIT, init)
}

function * init() {
    const userCredential: firebase.auth.UserCredential = JSON.parse(localStorage.getItem('user') || '{}');
    const token = localStorage.getItem('token');
    if(token){
        yield put(LoginAction.setLogin(userCredential.user?.email!, token ))
    }
}
function* login(action: ReturnType<typeof LoginAction.login>){
    try {

    const auth = firebase.auth()
    const userCredential:firebase.auth.UserCredential = yield call([auth, auth.signInWithEmailAndPassword], action.username, action.password);
    const idToken:string = yield call([userCredential.user, userCredential.user!.getIdToken])
    localStorage.setItem('user', JSON.stringify(userCredential));
    localStorage.setItem('token', idToken);
    yield put(LoginAction.setLogin(userCredential.user?.email!, idToken ))
    history.push('/orders');
    } catch (error) {
        NotificationManager.error('Could not login');
    }
}
function* logout() {
    const auth = firebase.auth();
    yield call([auth, auth.signOut]);
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    yield put(LoginAction.resetLogin());
}