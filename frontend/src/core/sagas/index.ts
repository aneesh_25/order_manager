import { all, fork, put } from "redux-saga/effects";
import { init } from "../actions/login.action";
import { watchInit, watchLogin, watchLogout } from "./login.saga";
import { watchAddOrder, watchGetOrder, watchGetOrders, watchUpdateOrder } from "./orders.saga";


export function* rootSaga() {
    yield all([
        fork(watchLogin),
        fork(watchLogout),
        fork(watchInit),
        fork(watchGetOrders),
        fork(watchGetOrder),
        fork(watchUpdateOrder),
        fork(watchAddOrder)
    ])
    yield put(init())
}