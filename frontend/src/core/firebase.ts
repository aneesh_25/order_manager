import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/app';
import 'firebase/firestore';

import firebaseConfig from './../config/firebaseConfig';

firebase.initializeApp(firebaseConfig);
firebase.auth()