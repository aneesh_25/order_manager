export const LOGIN = 'LOGIN' as const;
export const SET_LOGIN = 'SET_LOGIN' as const;
export const RESET_LOGIN = 'RESET_LOGIN' as const;
export const LOGOUT = 'LOGOUT' as const;
export const INIT = 'INIT' as const;
export const init = () => {
    return {
        type : INIT
    }
}
export const login = (username: string, password: string)=> {
    return {
        type: LOGIN,
        username,
        password
    } as const
}

export const logout = ()=> {
    return {
        type: LOGOUT,
    } as const
}

export const setLogin = (username: string, token: string) => {
    return {
        type : SET_LOGIN,
        username,
        token
    } as const
}

export const resetLogin = () => {
    return {
        type : RESET_LOGIN,
    } as const
}
export type LoginActionTypes = ReturnType<typeof login> |  ReturnType<typeof setLogin> |  ReturnType<typeof resetLogin> | ReturnType<typeof init>