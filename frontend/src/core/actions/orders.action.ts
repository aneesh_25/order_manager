import Order from "models";

export const GET_ORDERS = 'GET_ORDERS' as const;
export const SET_ORDERS = 'SET_ORDERS' as const;
export const GET_ORDER = 'GET_ORDER' as const;
export const SET_ORDER = 'SET_ORDER' as const;
export const UPDATE_ORDER = 'UPDATE_ORDER' as const;
export const ADD_ORDER = 'ADD_ORDER' as const;
export const getOrders = () => {
    return {
        type: GET_ORDERS
    }
}
export const setOrders = (orders: Order[]) => {
    return {
        type: SET_ORDERS,
        orders
    }
}
export const getOrder = (id: string) => {
    return {
        type: GET_ORDER,
        id
    }
}
export const updateOrder = (order: Order) => {
    return {
        type: UPDATE_ORDER,
        order
    }
}
export const addOrder = (order: Order) => {
    return {
        type: ADD_ORDER,
        order
    }
}
export const setOrder = (order: Order) => {
    return {
        type: SET_ORDER,
        order
    }
}

export type OrderActionsTypes =
    ReturnType<typeof getOrders>
    | ReturnType<typeof setOrders>
    | ReturnType<typeof getOrder>
    | ReturnType<typeof updateOrder>
    | ReturnType<typeof addOrder>
    | ReturnType<typeof setOrder>;
