import Order from "models"

type Props = {
    address: Order['address']
}
const Address: React.FunctionComponent<Props> = ({ address }) => {
    return (<span>{address?.street} {address?.city} {address?.country} {address?.zip} </span>)
}
export default Address;