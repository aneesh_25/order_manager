import Order from "models"

type Props = {
    customer: Order['customer']
}
const Customer: React.FunctionComponent<Props> = ({ customer }) => {
    return (<span>{customer?.name} {customer?.email}</span>)
}
export default Customer;