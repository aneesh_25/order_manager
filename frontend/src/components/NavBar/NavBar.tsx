import React from 'react';
import { Navbar as NavigationBar, Nav } from 'react-bootstrap';
type Props = {
  username: string,
  logout(): void;
}
const NavBar: React.FunctionComponent<Props> = ({
  username,
  logout
}) => {
  if (!username) {
    return null;
  }
  return (<>
    <NavigationBar bg="primary" variant="dark"
    >
      <NavigationBar.Brand>{username}</NavigationBar.Brand>
      <Nav className="justify-content-end">
        <Nav.Item>
          <Nav.Link onClick={() => { logout() }}
          >Logout</Nav.Link>
        </Nav.Item>
      </Nav>


    </NavigationBar>
  </>)
}
export default NavBar;