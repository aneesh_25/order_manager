import { connect} from 'react-redux';
import { Dispatch } from 'redux';
import { logout } from '../../core/actions/login.action';
import { RootState } from '../../core/reducers';
import NavBar from './NavBar';

const mapStateToProps = (state: RootState) => {
    return {
        username: state.login.user?.username!
    }
}
const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        logout: () => {
            dispatch(logout());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(NavBar)