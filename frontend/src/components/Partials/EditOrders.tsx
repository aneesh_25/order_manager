import Order from "models"
import React, { useEffect, useState } from "react"
import { Button, Form } from "react-bootstrap"

type Props = {
    order: Order,
    onUpdate(order: Order) : void;
}
const EditOrder: React.FunctionComponent<Props> = ({ order, onUpdate }) => {
    const [title, setTitle] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [city, setCity] = useState('');

    useEffect(() => {
        setTitle(order.title);
        setName(order.customer?.name!);
        setEmail(order.customer?.email!);
        setCity(order.address?.city!);
    }, [order])

    const onSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        onUpdate({
            ...order,
            title,
            address: { ...order.address, city },
            customer: { ...order.customer, name, email }
        })
    }
    return (
        <Form onSubmit={onSubmit}>
            <Form.Group >
                <Form.Label>Title</Form.Label>
                <Form.Control type="text" placeholder="Title" value={title} onChange={(e) => setTitle(e.target.value)} required/>
            </Form.Group>
            <Form.Group >
                <Form.Label>Customer Name</Form.Label>
                <Form.Control type="text" placeholder="Customer Name" value={name} onChange={(e) => setName(e.target.value)} />
            </Form.Group>
            <Form.Group >
                <Form.Label>Customer Email</Form.Label>
                <Form.Control type="email" placeholder="Customer Email" value={email} onChange={(e) => setEmail(e.target.value)} />
            </Form.Group>
            <Form.Group >
                <Form.Label>City</Form.Label>
                <Form.Control type="text" placeholder="City" value={city} onChange={(e) => setCity(e.target.value)} />
            </Form.Group>
            <Button variant="primary" type="submit">
                Submit
            </Button>
        </Form>
    )
}
export default EditOrder;