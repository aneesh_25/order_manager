import React from "react";
import { createStore } from "redux";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";

import reducer from "../src/core/reducers";
import { Router } from "react-router-dom";
import { createMemoryHistory} from 'history';

export function renderWithStore(ui, options) {
  const store = createStore(reducer);
  return {
    ...render(
      <Provider store={store}>
        {ui}
      </Provider>,
      options
    ),
    store,
  };
}

 
export function renderWithStoreAndThemeAndRouter(
    ui,
    route = '/',
    history = createMemoryHistory({ initialEntries: [route] })
   ) {
    const store = createStore(reducer);
    
    const Wrapper = ({ children }) => (
      <Provider store={store}>
          <Router history={history}>{children}</Router>
      </Provider>
    );
    
    return {
      ...render(ui, { wrapper: Wrapper }),
      store,
      history
    };
   }
   