import { RootState } from "./core/reducers";

export const selectToken = (state: RootState) => {
    return state.login.user?.token;
}