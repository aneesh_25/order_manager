import { useSelector } from "react-redux";
import { RootState } from "../core/reducers";

export default function useAuth() {
    const user = useSelector((state: RootState) => state.login.user)
    if(user){
        return true;
    }
    return false;
}