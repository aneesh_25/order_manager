import express from 'express'
import * as admin from 'firebase-admin';
import serviceAccount from '../serviceAccountKey';
import Order from 'models';
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://construyo-coding-challenge.firebaseio.com'
})
const app = express()
const port = process.env.PORT || 3001
app.use(express.json());

app.put('/api/orders/:id', async (req, res) => {
    try {
        await admin.auth().verifyIdToken(req.headers.authorization!);
        const db = admin.firestore();
        const order = req.body as Order;
        if (!order.title) {
            return res.status(400).send("Please provide a title");
        }
        await db.collection('orders').doc(order.id).update({
            ...order
        })
        return res.status(200).send()
    } catch (error) {
        res.status(500).send('Internal server error')
    }

})
app.post('/api/orders', async (req, res) => {
    try {
        await admin.auth().verifyIdToken(req.headers.authorization!);
        const db = admin.firestore();
        const order = req.body as Order;
        if (!order.title) {
            return res.status(400).send("Please provide a title");
        }
        await db.collection('orders').doc().set({
            ...order
        })
        return res.status(200).send()
    } catch (error) {
        res.status(500).send('Internal server error')
    }

})

app.listen(port, () => console.log(`Example app listening on port ${port}!`)) // tslint:disable-line no-console

